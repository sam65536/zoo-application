package springboot.zoo.domain.enums;

public enum UserRole {
    ROLE_USER, ROLE_ADMIN;
}