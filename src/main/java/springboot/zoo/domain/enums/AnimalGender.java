package springboot.zoo.domain.enums;

public enum AnimalGender {
    FEMALE, MALE;
}