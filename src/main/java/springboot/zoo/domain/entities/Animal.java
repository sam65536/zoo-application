package springboot.zoo.domain.entities;

import lombok.Getter;
import lombok.Setter;
import springboot.zoo.domain.enums.AnimalGender;
import springboot.zoo.domain.enums.AnimalType;


import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "animals")
public class Animal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated
    private AnimalGender gender;

    @Enumerated
    private AnimalType type;

    private String name;
    private Integer age;
    private String imagePath;

    @Lob
    private byte[] image;
}