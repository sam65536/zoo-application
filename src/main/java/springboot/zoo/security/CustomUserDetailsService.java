package springboot.zoo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import springboot.zoo.domain.entities.User;
import springboot.zoo.domain.enums.UserRole;
import springboot.zoo.exceptions.UserNotFoundException;
import springboot.zoo.repositories.UserRepository;

import java.util.HashSet;
import java.util.Set;

@Service
@Qualifier("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UserNotFoundException {
        try {
            User domainUser = userRepository.findByUsername(name);
            UserRole role = domainUser.getRole();
            Set<GrantedAuthority> authorities = new HashSet<>();
            authorities.add(new SimpleGrantedAuthority(role.toString()));
            return new CustomUserDetails(domainUser, authorities);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}