package springboot.zoo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springboot.zoo.config.SecurityConfig;
import springboot.zoo.domain.entities.User;
import springboot.zoo.domain.enums.UserRole;
import springboot.zoo.exceptions.UserNotFoundException;
import springboot.zoo.security.CustomUserDetails;
import springboot.zoo.security.CustomUserDetailsService;
import springboot.zoo.security.annotations.AllowedForAdmin;
import springboot.zoo.services.User.UserService;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final CustomUserDetailsService customUserDetailsService;

    @Autowired
    public UserController(UserService userService, AuthenticationManager authenticationManager, CustomUserDetailsService customUserDetailsService) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.customUserDetailsService = customUserDetailsService;
    }

    @GetMapping("/admin")
    @AllowedForAdmin
    public String admin(Model model) {
        model.addAttribute("users", userService.listAllUsers());
        return "users/admin-dashboard";
    }

    @GetMapping("/new")
    public String signUp(Model model) {
        model.addAttribute("user", new User());
        return "users/create";
    }

    @PostMapping
    public String signUp(@ModelAttribute User user) {
        try {
            user.setRole(UserRole.ROLE_USER);
            String password = user.getPassword();
            user.setPassword(SecurityConfig.encoder.encode(user.getPassword()));
            userService.saveUser(user);
            UserDetails userDetails = customUserDetailsService.loadUserByUsername(user.getUsername());
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
            authenticationManager.authenticate(auth);
            SecurityContextHolder.getContext().setAuthentication(auth);
            return "redirect:/users/me";
        } catch (Exception e) {
            return "error";
        }
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping(value = "{id}")
    @AllowedForAdmin
    public String showUserInfo(@PathVariable("id") int id, Model model) {
        User user = userService.getUserById(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        model.addAttribute("user", user);
        return "users/show";
    }

    @GetMapping("/me")
    public String showActiveProfile(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        CustomUserDetails myUser = (CustomUserDetails) authentication.getPrincipal();
        User user = userService.getUserById(myUser.getUser().getId());
        model.addAttribute("user", user);
        return "users/show";
    }


    @GetMapping("{id}/remove")
    @AllowedForAdmin
    public String remove(@PathVariable("id") Integer id) {
        userService.deleteUser(id);
        return "redirect:/users/admin";
	}

    @GetMapping("{id}/edit")
    @AllowedForAdmin
    public String edit(@PathVariable("id") Integer id, Model model) {
        User user = userService.getUserById(id);
        model.addAttribute("user", user);
        return "users/edit";
    }

    @PostMapping("/{id}")
    @AllowedForAdmin
    public String editSave(@ModelAttribute User user) {
        userService.saveUser(user);
        return "redirect:/users/admin";
    }

    @GetMapping("/checkUsername")
    public ResponseEntity<?> checkUsername(@RequestParam("username") String username) {
        User user = userService.getUserByUsername(username);
        ResponseEntity<?> result = (user == null) ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return result;
    }
}