package springboot.zoo.repositories;

import org.springframework.data.repository.CrudRepository;
import springboot.zoo.domain.entities.User;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);
}