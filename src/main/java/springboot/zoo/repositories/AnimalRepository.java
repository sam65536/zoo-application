package springboot.zoo.repositories;

import org.springframework.data.repository.CrudRepository;
import springboot.zoo.domain.entities.Animal;

public interface AnimalRepository extends CrudRepository<Animal, Integer> {
}