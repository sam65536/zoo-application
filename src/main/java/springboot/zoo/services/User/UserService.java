package springboot.zoo.services.User;

import springboot.zoo.domain.entities.User;

public interface UserService {
    Iterable<User> listAllUsers();

    User getUserById(Integer id);

    User getUserByUsername(String username);

    User saveUser(User user);

    void deleteUser(Integer id);
}
