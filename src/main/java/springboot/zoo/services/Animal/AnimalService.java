package springboot.zoo.services.Animal;

import springboot.zoo.domain.entities.Animal;

public interface AnimalService {
    Iterable<Animal> listAllAnimals();

    Animal getAnimalById(Integer id);

    Animal saveAnimal(Animal animal);

    void deleteAnimal(Integer id);
}